def fizzbuzz(n: int):
    for i in range(n):
        if i % 5 == 0:
            print("Fizz")
        elif i % 7 == 0:
            print("Buzz")
        elif i % 5 == 0 and n % 7 == 0:
            print("FizzBuzz")
        else:
            print(i)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="FizzBuzz")
    parser.add_argument('-n', type=int, default=100, help="Print up to this number")
    args = parser.parse_args()

    fizzbuzz(args.n)
